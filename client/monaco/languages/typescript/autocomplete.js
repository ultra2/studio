"use strict";
/// <reference path="../../../node_modules/monaco-editor/monaco.d.ts" />
exports.__esModule = true;
var global_1 = require("../../../global");
/**
 * Autocomplete
 */
var SuggestAdapter = (function () {
    function SuggestAdapter() {
    }
    SuggestAdapter.prototype.provideCompletionItems = function (model, position, token) {
        var wordInfo = model.getWordUntilPosition(position);
        var prefix = wordInfo.word;
        // NOTE: monaco is a bit touchy about `wordInfo`
        // e.g. for `test(|` it will  return `wordInfo.word == ""`.
        // We would rather it give us `(`.
        // Lets fix that:
        if (prefix == '' && wordInfo.startColumn > 2) {
            prefix = model.getLineContent(position.lineNumber).substr(wordInfo.startColumn - 2, 1);
        }
        // console.log({ prefix }); // DEBUG
        var filePath = model.uri.path;
        var offset = model.getOffsetAt(position);
        window["socket"].emit('main/editor/editor:getCompletionsAtPosition', {
            app: global_1["default"].application,
            prefix: prefix,
            filePath: filePath,
            position: offset
        });
        var promise = new Promise(function (resolve, reject) {
            window["socket"].on('getCompletionsAtPosition', function (msg) {
                var suggestions = msg.entries.map(function (entry) {
                    var result = {
                        label: entry.name,
                        kind: SuggestAdapter.convertKind(entry.kind)
                    };
                    return result;
                });
                resolve(suggestions);
            });
        });
        return promise;
    };
    SuggestAdapter.convertKind = function (kind) {
        switch (kind) {
            case Kind.primitiveType:
            case Kind.keyword:
                return monaco.languages.CompletionItemKind.Keyword;
            case Kind.variable:
            case Kind.localVariable:
                return monaco.languages.CompletionItemKind.Variable;
            case Kind.memberVariable:
            case Kind.memberGetAccessor:
            case Kind.memberSetAccessor:
                return monaco.languages.CompletionItemKind.Field;
            case Kind["function"]:
            case Kind.memberFunction:
            case Kind.constructSignature:
            case Kind.callSignature:
            case Kind.indexSignature:
                return monaco.languages.CompletionItemKind.Function;
            case Kind["enum"]:
                return monaco.languages.CompletionItemKind.Enum;
            case Kind.module:
                return monaco.languages.CompletionItemKind.Module;
            case Kind["class"]:
                return monaco.languages.CompletionItemKind.Class;
            case Kind.interface:
                return monaco.languages.CompletionItemKind.Interface;
            case Kind.warning:
            case Kind.file:
                return monaco.languages.CompletionItemKind.File;
        }
        return monaco.languages.CompletionItemKind.Property;
    };
    return SuggestAdapter;
}());
exports.SuggestAdapter = SuggestAdapter;
var Kind = (function () {
    function Kind() {
    }
    Kind.unknown = '';
    Kind.keyword = 'keyword';
    Kind.script = 'script';
    Kind.module = 'module';
    Kind["class"] = 'class';
    Kind.interface = 'interface';
    Kind.type = 'type';
    Kind["enum"] = 'enum';
    Kind.variable = 'var';
    Kind.localVariable = 'local var';
    Kind["function"] = 'function';
    Kind.localFunction = 'local function';
    Kind.memberFunction = 'method';
    Kind.memberGetAccessor = 'getter';
    Kind.memberSetAccessor = 'setter';
    Kind.memberVariable = 'property';
    Kind.constructorImplementation = 'constructor';
    Kind.callSignature = 'call';
    Kind.indexSignature = 'index';
    Kind.constructSignature = 'construct';
    Kind.parameter = 'parameter';
    Kind.typeParameter = 'type parameter';
    Kind.primitiveType = 'primitive type';
    Kind.label = 'label';
    Kind.alias = 'alias';
    Kind["const"] = 'const';
    Kind.let = 'let';
    Kind.warning = 'warning';
    Kind.file = 'file';
    return Kind;
}());
exports.Kind = Kind;
