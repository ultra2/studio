var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("global.client", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Global = (function () {
        function Global() {
            if (Global._instance) {
                throw new Error("The Global is a singleton class and cannot be created!");
            }
            Global._instance = this;
        }
        Global.getInstance = function () {
            return Global._instance;
        };
        Global._instance = new Global();
        return Global;
    }());
    var g = Global.getInstance();
    exports.default = g;
});
define("component.client", ["require", "exports", "react", "socket.io-client"], function (require, exports, React, io) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Component = (function (_super) {
        __extends(Component, _super);
        function Component(props) {
            return _super.call(this, props) || this;
        }
        Component.prototype.createSocket = function () {
            //window["socket"] = io.connect({ 
            //    extraHeaders: {
            //        'x-clientid': 'abc'
            //    }
            //})
            window["socket"] = io.connect(); //{ 
            //query: {
            //    app: window.location.pathname.match(/\/(\w*)\//)[1]
            //}
            //})
        };
        Component.prototype.on = function (message, handler) {
            window["socket"].on(message, handler.bind(this));
        };
        Component.prototype.emit = function (message, data, component) {
            if (component)
                message = component + ":" + message;
            window["socket"].emit(message, data);
        };
        return Component;
    }(React.Component));
    exports.default = Component;
});
define("navigator.client", ["require", "exports", "global.client", "component.client", "react", "react-bootstrap"], function (require, exports, global_client_1, component_client_1, React, react_bootstrap_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Navigator = (function (_super) {
        __extends(Navigator, _super);
        function Navigator(props) {
            var _this = _super.call(this, props) || this;
            _this.state = {
                info: {
                    workingUrl: ""
                },
                applications: []
            };
            _this.publishApplication = function () {
                this.clearLog();
                this.emit('publishApplication', { app: global_client_1.default.application }, "main/navigator/navigator");
            };
            _this.npminstallApplication = function () {
                this.clearLog();
                this.emit('npminstallApplication', { app: global_client_1.default.application }, "main/navigator/navigator");
            };
            _this.runApplication = function () {
                var url = '/' + global_client_1.default.application + '/Static/getFile/client/index.html';
                var win = window.open(url, '_blank');
                win.focus();
            };
            _super.prototype.on.call(_this, 'info', _this.setInfo);
            _super.prototype.on.call(_this, 'applications', _this.onApplications.bind(_this));
            _this.onChange = _this.onChange.bind(_this);
            _this.explorer_save = _this.explorer_save.bind(_this);
            _this.publishApplication = _this.publishApplication.bind(_this);
            _this.npminstallApplication = _this.npminstallApplication.bind(_this);
            _this.runApplication = _this.runApplication.bind(_this);
            return _this;
        }
        Navigator.prototype.setInfo = function (msg) {
            this.state.info = msg;
            this.setState(this.state);
        };
        Navigator.prototype.onApplications = function (msg) {
            this.state.applications = msg;
            this.setState(this.state);
        };
        //onSelect(eventKey: any, e?: React.SyntheticEvent<{}>) {
        //  this.state.application = eventKey
        //  this.emit('openApplication', this.state.application)
        //}
        Navigator.prototype.onChange = function (e) {
            global_client_1.default.application = e.target.value;
            _super.prototype.emit.call(this, 'openApplication', global_client_1.default.application, "main/navigator/navigator");
        };
        Navigator.prototype.explorer_save = function () {
            this.clearLog();
            this.saveFile();
        };
        Navigator.prototype.clearLog = function () {
            var el = document.getElementById("log");
            el.innerHTML = "";
        };
        Navigator.prototype.saveFile = function () {
            var model = monaco.editor.getModels()[0];
            var data = model.getValue();
            var dataBase64 = btoa(data);
            this.emit('saveFile', {
                app: global_client_1.default.application,
                _id: global_client_1.default.active["_id"],
                path: model.uri.path,
                content: dataBase64
            }, "main/navigator/navigator");
        };
        Navigator.prototype.createSelectItems = function () {
            var items = [];
            items.push(React.createElement("option", { value: "" }));
            for (var i = 0; i < this.state.applications.length; i++) {
                var app = this.state.applications[i];
                items.push(React.createElement("option", { value: app }, app));
            }
            return items;
        };
        Navigator.prototype.render = function () {
            var workingUrl = "DB: " + this.state.info["workingUrl"].substring(34);
            return (React.createElement(react_bootstrap_1.Navbar, { fluid: true, inverse: true, staticTop: true },
                React.createElement(react_bootstrap_1.Navbar.Form, null,
                    React.createElement(react_bootstrap_1.Button, { bsStyle: "primary", bsSize: "xsmall" },
                        React.createElement(react_bootstrap_1.Glyphicon, { glyph: "plus-sign" })),
                    ' ',
                    React.createElement(react_bootstrap_1.FormGroup, null,
                        React.createElement(react_bootstrap_1.ControlLabel, { className: "text-primary" }, "Open:"),
                        ' ',
                        React.createElement(react_bootstrap_1.FormControl, { componentClass: "select", placeholder: "select", onChange: this.onChange }, this.createSelectItems())),
                    ' ',
                    React.createElement(react_bootstrap_1.Button, { bsStyle: "success", onClick: this.explorer_save },
                        React.createElement(react_bootstrap_1.Glyphicon, { glyph: "floppy-save" }),
                        " Save All"),
                    ' ',
                    React.createElement(react_bootstrap_1.Button, { bsStyle: "success", onClick: this.publishApplication, bsSize: "xsmall" },
                        React.createElement(react_bootstrap_1.Glyphicon, { glyph: "play-circle" }),
                        " Publish"),
                    ' ',
                    React.createElement(react_bootstrap_1.Button, { bsStyle: "success", onClick: this.runApplication, bsSize: "xsmall" },
                        React.createElement(react_bootstrap_1.Glyphicon, { glyph: "play" }),
                        " Run"),
                    ' ',
                    React.createElement(react_bootstrap_1.Button, { bsStyle: "success", onClick: this.npminstallApplication, bsSize: "xsmall" },
                        React.createElement(react_bootstrap_1.Glyphicon, { glyph: "piggy-bank" }),
                        " npm install"),
                    ' ',
                    React.createElement(react_bootstrap_1.Label, { bsStyle: "primary" }, this.state.info.workingUrl),
                    "\u00A0")));
        };
        return Navigator;
    }(component_client_1.default));
    exports.default = Navigator;
});
define("explorer.client", ["require", "exports", "global.client", "component.client", "react", "react-bootstrap", "react-ui-tree", "classnames"], function (require, exports, global_client_2, component_client_2, React, react_bootstrap_2, Tree, cx) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Explorer = (function (_super) {
        __extends(Explorer, _super);
        function Explorer(props) {
            var _this = _super.call(this, props) || this;
            _this.state = {
                active: null,
                tree: null
            };
            _this.newFolder = _this.newFolder.bind(_this);
            _this.newFile = _this.newFile.bind(_this);
            _this.handleChange = _this.handleChange.bind(_this);
            _this.onClickNode = _this.onClickNode.bind(_this);
            _this.onDoubleClickNode = _this.onDoubleClickNode.bind(_this);
            _this.renderNode = _this.renderNode.bind(_this);
            _super.prototype.on.call(_this, 'application', _this.onApplication);
            _super.prototype.on.call(_this, 'newFolder', _this.onNewFolder);
            _super.prototype.on.call(_this, 'newFile', _this.onNewFile);
            return _this;
        }
        Explorer.prototype.newFolder = function () {
            var name = prompt("Folder name:");
            if (!name)
                return;
            var path = global_client_2.default.active["path"] + '/' + name;
            this.emit('newFolder', { app: global_client_2.default.application, path: path }, "main/explorer/explorer");
        };
        Explorer.prototype.newFile = function () {
            var name = prompt("File name:");
            if (!name)
                return;
            var path = global_client_2.default.active["path"] + '/' + name;
            this.emit('newFile', { app: global_client_2.default.application, path: path }, "main/explorer/explorer");
        };
        Explorer.prototype.editFile = function () {
            this.emit('editFile', { app: global_client_2.default.application, path: global_client_2.default.active["path"] }, "main/explorer/explorer");
        };
        Explorer.prototype.onApplication = function (msg) {
            this.state.tree = msg["tree"];
            this.setState(this.state);
        };
        Explorer.prototype.onNewFolder = function (msg) {
            global_client_2.default.active["children"].push(msg.file);
            this.setState(this.state);
        };
        Explorer.prototype.onNewFile = function (msg) {
            global_client_2.default.active["children"].push(msg.file);
            this.setState(this.state);
        };
        Explorer.prototype.handleChange = function (tree) {
            this.state.tree = tree;
            this.setState(this.state);
        };
        Explorer.prototype.onClickNode = function (node) {
            this.state.active = node;
            this.setState(this.state);
            global_client_2.default.active = node;
        };
        Explorer.prototype.onDoubleClickNode = function (node) {
            this.state.active = node;
            this.setState(this.state);
            global_client_2.default.active = node;
            this.editFile();
        };
        Explorer.prototype.renderNode = function (node) {
            return (React.createElement("span", { className: cx('node', { 'is-active': node === this.state.active }), onClick: this.onClickNode.bind(null, node), onDoubleClick: this.onDoubleClickNode.bind(null, node) }, node.filename));
        };
        Explorer.prototype.render = function () {
            return (React.createElement("div", { className: "explorer" },
                React.createElement("div", { className: "explorer-commandbar" },
                    React.createElement(react_bootstrap_2.Button, { bsStyle: "default", bsSize: "xsmall", onClick: this.newFolder },
                        React.createElement(react_bootstrap_2.Glyphicon, { glyph: "folder-close" })),
                    ' ',
                    React.createElement(react_bootstrap_2.Button, { bsStyle: "primary", bsSize: "xsmall", onClick: this.newFile },
                        React.createElement(react_bootstrap_2.Glyphicon, { glyph: "file" }))),
                React.createElement(Tree, { paddingLeft: 20, tree: this.state.tree, onChange: this.handleChange, renderNode: this.renderNode })));
        };
        return Explorer;
    }(component_client_2.default));
    exports.default = Explorer;
});
//<div className="inspector">
//  <pre>
//    {JSON.stringify(this.state.tree, null, '  ')}
//  </pre>
//</div> 
/// <reference path="../../../node_modules/monaco-editor/monaco.d.ts" />
define("monaco/languages/typescript/autocomplete", ["require", "exports", "global.client"], function (require, exports, global_client_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * Autocomplete
     */
    var SuggestAdapter = (function () {
        function SuggestAdapter() {
        }
        SuggestAdapter.prototype.provideCompletionItems = function (model, position, token) {
            var wordInfo = model.getWordUntilPosition(position);
            var prefix = wordInfo.word;
            // NOTE: monaco is a bit touchy about `wordInfo`
            // e.g. for `test(|` it will  return `wordInfo.word == ""`.
            // We would rather it give us `(`.
            // Lets fix that:
            if (prefix == '' && wordInfo.startColumn > 2) {
                prefix = model.getLineContent(position.lineNumber).substr(wordInfo.startColumn - 2, 1);
            }
            // console.log({ prefix }); // DEBUG
            var filePath = model.uri.path;
            var offset = model.getOffsetAt(position);
            window["socket"].emit('main/editor/editor:getCompletionsAtPosition', {
                app: global_client_3.default.application,
                prefix: prefix,
                filePath: filePath,
                position: offset
            });
            var promise = new Promise(function (resolve, reject) {
                window["socket"].on('getCompletionsAtPosition', function (msg) {
                    var suggestions = msg.entries.map(function (entry) {
                        var result = {
                            label: entry.name,
                            kind: SuggestAdapter.convertKind(entry.kind)
                        };
                        return result;
                    });
                    resolve(suggestions);
                });
            });
            return promise;
        };
        SuggestAdapter.convertKind = function (kind) {
            switch (kind) {
                case Kind.primitiveType:
                case Kind.keyword:
                    return monaco.languages.CompletionItemKind.Keyword;
                case Kind.variable:
                case Kind.localVariable:
                    return monaco.languages.CompletionItemKind.Variable;
                case Kind.memberVariable:
                case Kind.memberGetAccessor:
                case Kind.memberSetAccessor:
                    return monaco.languages.CompletionItemKind.Field;
                case Kind.function:
                case Kind.memberFunction:
                case Kind.constructSignature:
                case Kind.callSignature:
                case Kind.indexSignature:
                    return monaco.languages.CompletionItemKind.Function;
                case Kind.enum:
                    return monaco.languages.CompletionItemKind.Enum;
                case Kind.module:
                    return monaco.languages.CompletionItemKind.Module;
                case Kind.class:
                    return monaco.languages.CompletionItemKind.Class;
                case Kind.interface:
                    return monaco.languages.CompletionItemKind.Interface;
                case Kind.warning:
                case Kind.file:
                    return monaco.languages.CompletionItemKind.File;
            }
            return monaco.languages.CompletionItemKind.Property;
        };
        return SuggestAdapter;
    }());
    exports.SuggestAdapter = SuggestAdapter;
    var Kind = (function () {
        function Kind() {
        }
        Kind.unknown = '';
        Kind.keyword = 'keyword';
        Kind.script = 'script';
        Kind.module = 'module';
        Kind.class = 'class';
        Kind.interface = 'interface';
        Kind.type = 'type';
        Kind.enum = 'enum';
        Kind.variable = 'var';
        Kind.localVariable = 'local var';
        Kind.function = 'function';
        Kind.localFunction = 'local function';
        Kind.memberFunction = 'method';
        Kind.memberGetAccessor = 'getter';
        Kind.memberSetAccessor = 'setter';
        Kind.memberVariable = 'property';
        Kind.constructorImplementation = 'constructor';
        Kind.callSignature = 'call';
        Kind.indexSignature = 'index';
        Kind.constructSignature = 'construct';
        Kind.parameter = 'parameter';
        Kind.typeParameter = 'type parameter';
        Kind.primitiveType = 'primitive type';
        Kind.label = 'label';
        Kind.alias = 'alias';
        Kind.const = 'const';
        Kind.let = 'let';
        Kind.warning = 'warning';
        Kind.file = 'file';
        return Kind;
    }());
    exports.Kind = Kind;
});
define("editor.client", ["require", "exports", "global.client", "component.client", "react", "monaco/languages/typescript/autocomplete"], function (require, exports, global_client_4, component_client_3, React, autocomplete_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    //import monaco = require('monaco-editor')
    //import CompletionItemProvider = monaco.languages.CompletionItemProvider;
    var Editor = (function (_super) {
        __extends(Editor, _super);
        function Editor(props) {
            var _this = _super.call(this, props) || this;
            _this.state = {};
            _super.prototype.on.call(_this, 'editFile', _this.onEditFile);
            window["require"].config({ paths: { 'vs': 'monaco-editor/min/vs' } });
            window["require"](['vs/editor/editor.main'], function () {
                global_client_4.default.editor = monaco.editor.create(document.getElementById('editor'), {
                    language: 'typescript',
                    folding: true,
                    theme: 'vs-dark',
                    model: null
                });
                monaco.languages.typescript.typescriptDefaults.setCompilerOptions({
                    target: monaco.languages.typescript.ScriptTarget.ES5,
                    allowNonTsExtensions: true,
                    moduleResolution: monaco.languages.typescript.ModuleResolutionKind.NodeJs,
                    module: monaco.languages.typescript.ModuleKind.CommonJS,
                    noEmit: true,
                    jsx: 2
                });
                var suggestAdapter = new autocomplete_1.SuggestAdapter();
                monaco.languages.registerCompletionItemProvider('typescript', suggestAdapter);
            });
            return _this;
        }
        Editor.prototype.onEditFile = function (msg) {
            var models = monaco.editor.getModels();
            for (var i = 0; i < models.length; i++) {
                var model = models[i];
                model.dispose();
            }
            var contentType = global_client_4.default.active["contentType"];
            var editorMode = contentType.substring(contentType.indexOf('/') + 1);
            var model = monaco.editor.createModel(msg.content, editorMode, monaco.Uri.parse(msg.path));
            global_client_4.default.editor.setModel(model);
        };
        Editor.prototype.render = function () {
            return (React.createElement("div", { id: "editor", className: "col-sm-12 editor" }));
        };
        return Editor;
    }(component_client_3.default));
    exports.default = Editor;
});
define("main.client", ["require", "exports", "global.client", "component.client", "navigator.client", "explorer.client", "editor.client", "react", "react-dom", "react-split-pane"], function (require, exports, global_client_5, component_client_4, navigator_client_1, explorer_client_1, editor_client_1, React, ReactDOM, SplitPane) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Main = (function (_super) {
        __extends(Main, _super);
        function Main(props) {
            var _this = _super.call(this, props) || this;
            _super.prototype.createSocket.call(_this);
            _super.prototype.on.call(_this, 'main:log', _this.writeLog);
            global_client_5.default.application = "";
            return _this;
        }
        Main.prototype.writeLog = function (message) {
            var el = document.getElementById("log");
            el.innerHTML = el.innerHTML + "<br/>" + message;
        };
        Main.prototype.render = function () {
            return (React.createElement("div", null,
                React.createElement(navigator_client_1.default, null),
                React.createElement(SplitPane, { split: "vertical", minSize: 50, defaultSize: parseInt(localStorage.getItem('splitPosVertical'), 10), onChange: function (size) { return localStorage.setItem('splitPosVertical', size); } },
                    React.createElement(explorer_client_1.default, null),
                    React.createElement(SplitPane, { split: "horizontal", minSize: 50, defaultSize: parseInt(localStorage.getItem('splitPosHorizontal'), 10), onChange: function (size) { return localStorage.setItem('splitPosHorizontal', size); } },
                        React.createElement(editor_client_1.default, null),
                        React.createElement(SplitPane, { split: "vertical", minSize: 50, defaultSize: parseInt(localStorage.getItem('splitPosVertical2'), 10), onChange: function (size) { return localStorage.setItem('splitPosVertical2', size); } },
                            React.createElement("div", { className: "log" },
                                React.createElement("div", { id: "log" })),
                            React.createElement("div", { className: "console" },
                                React.createElement("div", { id: "console" })))))));
        };
        return Main;
    }(component_client_4.default));
    exports.Main = Main;
    ReactDOM.render(React.createElement(Main), document.getElementById('root'));
});
