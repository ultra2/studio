"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const global_server_1 = require("./global.server");
const component_server_1 = require("./component.server");
//#COMMON END
const mongodb = require("mongodb");
const gridfs = require("gridfs-stream");
class Main extends component_server_1.default {
    connect(data) {
        return __awaiter(this, void 0, void 0, function* () {
            global_server_1.default.db = yield mongodb.MongoClient.connect(process.env.workingUrl);
            global_server_1.default.gridfs = gridfs(global_server_1.default.db, mongodb);
            var applications = yield this.listApplications();
            process.send({ message: 'applications', data: applications });
            process.send({ message: 'info', data: { workingUrl: process.env.workingUrl.substring(process.env.workingUrl.indexOf('@') + 1) } });
        });
    }
    listApplications() {
        return __awaiter(this, void 0, void 0, function* () {
            var result = [];
            var data = yield global_server_1.default.db.listCollections({}).toArray();
            data.forEach(function (element, index) {
                return __awaiter(this, void 0, void 0, function* () {
                    //var name = element.name.split('.')[0]
                    //if (name == "system") return
                    if (element.name.indexOf('.') != -1)
                        return;
                    result.push(element.name);
                });
            }.bind(this));
            return result;
        });
    }
}
exports.default = Main;
