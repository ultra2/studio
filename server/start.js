"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
console.log("studio44 started...");
const fsextra = require("fs-extra");
const pathhelper = require("path");
//EXPRESS ROUTES
var express = require('express');
var app = express();
var fs = require('fs');
app.use(express.static('./client'));
app.get("/", function (req, res) {
    res.send("ok studio");
    res.end();
});
//WEB SERVER
var http = require('http');
//var https = require('https');
var server = http.createServer(app);
//var server = https.createServer(options, app).listen(443);
//ar _counter = 0
//WEB SOCKET
var io = require('socket.io')(server);
io.on('connection', function (socket) {
    console.log("studio44: new client socket connected");
    //function onMessage(message) {
    //	switch (message.command) {
    //		case "applications":
    //			socket.emit('applications', message.data)
    //			break
    //	}
    //}
    //process.on('message', onMessage.bind(this))
    //process.send({ command: "applications", data: {} })
    var livePath = "/tmp/live";
    var applications = [];
    fsextra.ensureDir(livePath);
    var directories = fsextra.readdirSync(livePath);
    for (var i in directories) {
        var dir = directories[i];
        if (!fsextra.lstatSync(pathhelper.join(livePath, dir)).isDirectory())
            continue;
        applications.push(dir);
    }
    socket.emit('applications', applications);
    socket.use((params, next) => {
        var message = params[0];
        var splittedMessage = message.split(':');
        var component = splittedMessage[0];
        var method = splittedMessage[1];
        var data = params[1];
        if (component.lastIndexOf('/') != -1) {
            component = component.substr(component.lastIndexOf('/') + 1);
        }
        var componentModule = require('./' + component + '.server');
        var componentInstance = new componentModule.default();
        //    componentInstance["db"] = this.db
        //    componentInstance["gridfs"] = this.gridfs
        componentInstance["emitfn"] = function (message, data) {
            socket.emit(message, data);
        };
        //experimental
        //    componentInstance["session"] = socket["session"]
        componentInstance[method](data);
        return next();
    });
    socket.on('disconnect', function () { });
});
var host = process.env.IP || "127.0.0.1";
var port = parseInt(process.env.PORT) || 8080;
server.listen(port, host, function () {
    console.log('studio started on %s:%d ...', host, port);
});
