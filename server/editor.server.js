"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const global_server_1 = require("./global.server");
const component_server_1 = require("./component.server");
class Editor extends component_server_1.default {
    getCompletionsAtPosition(data) {
        return __awaiter(this, void 0, void 0, function* () {
            var languageService = null;
            if (data.filePath.indexOf('.server.') != -1) {
                languageService = global_server_1.default.languageServiceServer;
                //data.filePath = data.filePath.replace('.server', '')
            }
            else {
                languageService = global_server_1.default.languageServiceClient;
            }
            const completions = languageService.getCompletionsAtPosition(data.filePath, data.position);
            //TODO: duplikacios hiba javitasa
            //for (var i=0; i<completions.entries.length; i++){
            //    var c = completions.entries[i]
            //    if (c.name == "Navigator"){
            //        debugger
            //    }
            //}
            let completionList = completions || {};
            completionList["entries"] = completionList["entries"] || [];
            // limit to maxSuggestions
            let maxSuggestions = 1000;
            if (completionList["entries"].length > maxSuggestions)
                completionList["entries"] = completionList["entries"].slice(0, maxSuggestions);
            this.emit("getCompletionsAtPosition", completionList);
        });
    }
}
exports.default = Editor;
