"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const global_server_1 = require("./global.server");
const component_server_1 = require("./component.server");
//#COMMON END
const fsextra = require("fs-extra");
class Explorer extends component_server_1.default {
    constructor() {
        super();
    }
    //---------------------------------------
    //SOCKET BEGIN
    //---------------------------------------
    newFolder(data) {
        return __awaiter(this, void 0, void 0, function* () {
            fsextra.mkdirpSync(global_server_1.default.repo + data.app + '/' + data.path);
            var file = global_server_1.default.navigator.createNode(data.app, data.path);
            this.emit("newFolder", { file: file });
        });
    }
    newFile(data) {
        return __awaiter(this, void 0, void 0, function* () {
            fsextra.writeFileSync(global_server_1.default.repo + data.app + '/' + data.path, "", { flag: 'w' });
            var file = global_server_1.default.navigator.createNode(data.app, data.path);
            this.emit("newFile", { file: file });
        });
    }
    editFile(data) {
        return __awaiter(this, void 0, void 0, function* () {
            var buffer = fsextra.readFileSync(global_server_1.default.repo + data.app + '/' + data.path);
            this.emit("editFile", { path: data.path, content: buffer.toString() });
        });
    }
}
exports.default = Explorer;
