"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Global {
    constructor() {
        this.repo = "/tmp/live/";
        this.navigator = null;
        this.gitLabAccessToken = "k5T9xs82anhKt1JKaM39";
        if (Global._instance) {
            throw new Error("The Global is a singleton class and cannot be created!");
        }
        Global._instance = this;
    }
    static getInstance() {
        return Global._instance;
    }
}
Global._instance = new Global();
var g = Global.getInstance();
exports.default = g;
