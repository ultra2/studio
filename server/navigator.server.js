"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const global_server_1 = require("./global.server");
const component_server_1 = require("./component.server");
//#COMMON END
const ts = require("typescript");
const pathhelper = require("path");
const mime = require("mime");
const fsextra = require("fs-extra");
const uuid = require("uuid");
const moment = require("moment");
var npmi = require('npmi');
var Git = require("nodegit");
var gitkit = require('nodegit-kit');
class Navigator extends component_server_1.default {
    constructor() {
        super();
        global_server_1.default.navigator = this;
    }
    //---------------------------------------
    //SOCKET BEGIN
    //---------------------------------------
    openApplication(data) {
        return __awaiter(this, void 0, void 0, function* () {
            var app = data;
            this.open(app);
            //this.session["languageServiceHostServer"] = new LanguageServiceHost(app, "server")
            //this.session["languageServiceHostClient"] = new LanguageServiceHost(app, "client")
            //this.session["languageServiceServer"] = ts.createLanguageService(this.session["languageServiceHostServer"], ts.createDocumentRegistry());
            //this.session["languageServiceClient"] = ts.createLanguageService(this.session["languageServiceHostClient"], ts.createDocumentRegistry());
            global_server_1.default.languageServiceHostServer = new LanguageServiceHost(app, "server");
            global_server_1.default.languageServiceHostClient = new LanguageServiceHost(app, "client");
            global_server_1.default.languageServiceServer = ts.createLanguageService(global_server_1.default.languageServiceHostServer, ts.createDocumentRegistry());
            global_server_1.default.languageServiceClient = ts.createLanguageService(global_server_1.default.languageServiceHostClient, ts.createDocumentRegistry());
        });
    }
    publishApplication(data) {
        return __awaiter(this, void 0, void 0, function* () {
            var success = yield this.compile(data.app);
            if (!success)
                return;
            yield this.push(data.app);
            //await this.build(data.app)
            //await this.publishToDb(data.app)
        });
    }
    npminstallApplication(data) {
        return __awaiter(this, void 0, void 0, function* () {
            this.npminstall(data.app);
        });
    }
    saveFile(data) {
        return __awaiter(this, void 0, void 0, function* () {
            var content = new Buffer(data.content, 'base64').toString();
            fsextra.writeFileSync(global_server_1.default.repo + data.app + "/" + data.path, content, { flag: 'w' });
            this.log("saved: " + data.path);
            this.compile(data.app);
        });
    }
    //---------------------------------------
    //SOCKET END
    //---------------------------------------
    open(app) {
        return __awaiter(this, void 0, void 0, function* () {
            this.log("open...");
            //if (!fsextra.existsSync(g.repo + app)){
            //    await this.clone(app)
            //    await this.npminstall(app)
            //}
            //else{
            //    await this.update(app)
            //}
            var root = this.createNode(app, '');
            this.emit("application", {
                name: app,
                tree: root
            });
        });
    }
    clone(app) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                this.log("clone...");
                var repossh = yield this.getRepositoryUrl(app);
                //var cloneOptions = { fetchOpts: { callbacks: this.engine.getRemoteCallbacks() } }
                var repo = yield Git.Clone(repossh, global_server_1.default.repo + app);
                this.emit("log", "clone success", "main");
                return repo;
            }
            catch (err) {
                console.log(err);
                this.emit("log", err.message, "main");
                throw err;
            }
        });
    }
    update(app) {
        return __awaiter(this, void 0, void 0, function* () {
            this.emit("log", "update...", "main");
            var repo = yield Git.Repository.open(global_server_1.default.repo + app);
            yield repo.fetchAll();
            var signature = this.getSignature();
            yield repo.mergeBranches("master", "origin/master", signature, null, { fileFavor: Git.Merge.FILE_FAVOR.THEIRS });
            this.emit("log", "update success", "main");
            return repo;
        });
    }
    getSignature() {
        return Git.Signature.create("Foo bar", "foo@bar.com", 123456789, 60);
    }
    getRepositorySsh(app) {
        return __awaiter(this, void 0, void 0, function* () {
            var registry = (yield global_server_1.default.db.collection(app).find().toArray())[0];
            return registry.repository.ssh;
        });
    }
    getRepositoryUrl(app) {
        return __awaiter(this, void 0, void 0, function* () {
            var registry = (yield global_server_1.default.db.collection(app).find().toArray())[0];
            return registry.repository.url.replace("https://", "https://oauth2:" + global_server_1.default.gitLabAccessToken + "@");
        });
    }
    createNode(app, relpath) {
        var node = {};
        node["filename"] = (relpath == '') ? app : pathhelper.basename(relpath);
        node["collapsed"] = (relpath != '');
        node["path"] = relpath;
        var stat = fsextra.lstatSync(global_server_1.default.repo + app + '/' + relpath);
        if (stat.isFile()) {
            node["contentType"] = this.getMime(relpath);
        }
        if (stat.isDirectory()) {
            node["contentType"] = "text/directory";
            node["children"] = [];
            var children = fsextra.readdirSync(global_server_1.default.repo + app + '/' + relpath);
            children = children.sort();
            for (var i in children) {
                var child = children[i];
                if (child[0] == '.')
                    continue;
                if (child == 'node_modules')
                    continue;
                var childPath = (relpath) ? relpath + '/' + child : child;
                var childNode = this.createNode(app, childPath);
                node["children"].push(childNode);
            }
        }
        return node;
    }
    compile(app) {
        return __awaiter(this, void 0, void 0, function* () {
            var serverSuccess = yield this.compileStep(app, "server");
            var clientSuccess = yield this.compileStep(app, "client");
            return serverSuccess && clientSuccess;
        });
    }
    compileStep(app, mode) {
        return __awaiter(this, void 0, void 0, function* () {
            const languageService = (mode == 'server') ? global_server_1.default.languageServiceServer : global_server_1.default.languageServiceClient;
            this.emit("log", "compile " + mode + "...", "main");
            let program = languageService.getProgram();
            let emitResult = program.emit(undefined, function (fileName, data, writeByteOrderMark, onError, sourceFiles) {
                fsextra.writeFileSync(global_server_1.default.repo + app + '/' + fileName, data, { flag: 'w' });
            }.bind(this));
            //let allDiagnostics = ts.getPreEmitDiagnostics(program).concat(emitResult.diagnostics)
            let allDiagnostics = emitResult.diagnostics;
            allDiagnostics.forEach(diagnostic => {
                let { line, character } = diagnostic.file.getLineAndCharacterOfPosition(diagnostic.start);
                let message = ts.flattenDiagnosticMessageText(diagnostic.messageText, '\n');
                this.emit("log", `${diagnostic.file.fileName} (${line + 1},${character + 1}): ${message}`, "main");
            });
            let success = !emitResult.emitSkipped;
            if (!success) {
                this.emit("log", "compile " + mode + " failed", "main");
            }
            else {
                this.emit("log", "compile " + mode + " success", "main");
            }
            return success;
        });
    }
    npminstall(app) {
        return __awaiter(this, void 0, void 0, function* () {
            this.emit("log", "npm install...", "main");
            console.log("npm install...");
            var options = {
                //name: 'react-split-pane',	// your module name
                //version: '3.10.9',		// expected version [default: 'latest']
                path: global_server_1.default.repo + app,
                forceInstall: false,
                npmLoad: {
                    loglevel: 'warn' // [default: {loglevel: 'silent'}]
                }
            };
            function install(resolve, reject) {
                npmi(options, function (err, result) {
                    if (err) {
                        if (err.code === npmi.LOAD_ERR) {
                            console.log('npm load error');
                            this.emit("log", "npm install: load error", "main");
                            reject(err);
                            return;
                        }
                        if (err.code === npmi.INSTALL_ERR) {
                            console.log('npm install error: ' + err.message);
                            this.emit("log", "npm install: " + err.message, "main");
                            reject(err);
                            return;
                        }
                        console.log(err.message);
                        this.emit("log", "npm install: " + err.message, "main");
                        reject(err);
                    }
                    console.log('npm install success');
                    this.emit("log", "npm install success", "main");
                    resolve(result);
                }.bind(this));
            }
            return new Promise(install.bind(this));
        });
    }
    push(app) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                this.log("push...");
                var repo = yield Git.Repository.open(global_server_1.default.repo + app);
                yield gitkit.config.set(repo, {
                    'user.name': 'John Doe',
                    'user.email': 'johndoe@example.com'
                });
                var diff = yield gitkit.diff(repo);
                //console.log(diff)
                yield gitkit.commit(repo, {
                    'message': 'commit message'
                });
                var log = yield gitkit.log(repo);
                //console.log(log)
                //index
                //var index = await repo.refreshIndex()
                //var index = await repo.index()
                //var a = await index.removeAll()
                //var a2 =await index.addAll()
                //var a3 =await index.write()
                //var oid = await index.writeTree()
                //commit
                //await repo.createCommit("HEAD", signature, signature, "initial commit", oid, [])
                //remote
                var remote = yield Git.Remote.lookup(repo, "origin");
                if (remote == null) {
                    var repourl = yield this.getRepositoryUrl(app);
                    remote = yield Git.Remote.create(repo, "origin", repourl);
                }
                //push
                //await remote.push(["refs/heads/master:refs/heads/master"], { callbacks: this.engine.getRemoteCallbacks() })
                yield remote.push(["refs/heads/master:refs/heads/master"]);
                this.log("push success");
            }
            catch (err) {
                console.log(err);
            }
        });
    }
    publishToDb(app) {
        return __awaiter(this, void 0, void 0, function* () {
            this.log("publish...");
            var paths = [];
            this.publishNodeToDb("dist", paths, app);
            yield Promise.all(paths.map((path) => __awaiter(this, void 0, void 0, function* () { yield this.publishFileToDb(path, app); })));
            //TODO
            //await this.cacheServer()
            //    var buffer = fsextra.readFileSync(config.output.path + '/' + config.output.filename)
            //    await this.engine.mongo.uploadFileOrFolder(config.output.path.substr('/tmp/virtual/'.length) + '/' + config.output.filename, buffer) 
            this.log("publish success.");
        });
    }
    publishNodeToDb(path, paths, app) {
        var stat = fsextra.lstatSync(global_server_1.default.repo + app + '/' + path);
        if (stat.isFile()) {
            paths.push(path);
            return;
        }
        if (stat.isDirectory()) {
            var children = fsextra.readdirSync(global_server_1.default.repo + app + '/' + path);
            for (var i in children) {
                var child = children[i];
                if (child[0] == '.')
                    continue;
                var childPath = (path) ? path + '/' + child : child;
                this.publishNodeToDb(childPath, paths, app);
            }
            return;
        }
    }
    publishFileToDb(path, app) {
        return __awaiter(this, void 0, void 0, function* () {
            var buffer = fsextra.readFileSync(global_server_1.default.repo + app + '/' + path);
            var pathToSave = path.substr(5); //remove 'dist/', we dont need it in db
            yield this.dbSaveFile(pathToSave, buffer, app);
            //if (socket) socket.emit("log", "Publish file: " + path)
        });
    }
    loadFile(path, app) {
        var result = new fileInfo();
        result.buffer = fsextra.readFileSync(global_server_1.default.repo + app + '/' + path);
        result.contentType = this.getMime(path);
        //this.engine.io.sockets.emit('log', path + " load: " + result.buffer.toString().length)
        return result;
    }
    dbSaveFile(path, content, app) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var filedesc = yield global_server_1.default.db.collection(app + ".files").findOne({ filename: path });
                var _id = (filedesc) ? filedesc._id : uuid.v1();
                var writestream = global_server_1.default.gridfs.createWriteStream({
                    _id: _id,
                    filename: path,
                    content_type: this.getMime(path),
                    metadata: {
                        modified: moment().format('YYYY-MM-DD HH:mm:ss Z')
                    },
                    root: app
                });
                yield this.toStream(content, writestream);
            }
            catch (err) {
                throw Error(err.message);
            }
        });
    }
    getMime(path) {
        var ext = pathhelper.extname(path);
        if (ext == ".ts")
            return "application/typescript";
        if (ext == ".tsx")
            return "application/typescript";
        return mime.lookup(path);
    }
    toStream(data, writestream) {
        writestream.write(data);
        writestream.end();
        function doit(resolve, reject) {
            writestream.on('close', function (result) {
                resolve(result);
            });
            writestream.on('error', function (err) {
                reject(err.message);
            });
        }
        return new Promise(doit);
    }
}
exports.default = Navigator;
class LanguageServiceHost {
    constructor(_app, _mode) {
        this.mode = _mode;
        this.app = _app;
        this.repo = "/tmp/live/" + this.app;
    }
    getScriptFileNames() {
        return (this.mode == 'server') ? ["start.ts", "main.server.ts"] : ["main.client.tsx"];
    }
    getScriptVersion(path) {
        path = this.fixPath(path);
        var stat = fsextra.lstatSync(path);
        return stat.mtime.toString();
    }
    getScriptSnapshot(fileName) {
        if (!this.fileExists(fileName))
            return undefined;
        var source = this.readFile(fileName);
        return ts.ScriptSnapshot.fromString(source);
    }
    getCurrentDirectory() {
        return ".";
    }
    getCompilationSettings() {
        var path = '/config/tsconfig-' + this.mode + '.json';
        if (fsextra.existsSync(this.repo + "/" + path)) {
            var tsconfig = fsextra.readFileSync(this.repo + '/' + path).toString();
            var result = JSON.parse(tsconfig);
            return result;
        }
        //return { 
        //    outFile: "dist/client/main-all.js",
        //    noEmitOnError: true, 
        //    noImplicitAny: false,
        //    target: ts.ScriptTarget.ES2016, 
        //    module: ts.ModuleKind.AMD,
        //    jsx: ts.JsxEmit.React
        //}
    }
    getDefaultLibFileName(options) {
        return ts.getDefaultLibFilePath(options);
    }
    readFile(path, encoding) {
        path = this.fixPath(path);
        return fsextra.readFileSync(path).toString();
    }
    fileExists(path) {
        path = this.fixPath(path);
        var result = fsextra.existsSync(path);
        //console.log("fileExists: " + path + ": " + result) 
        return result;
    }
    fixPath(path) {
        //    path = (this.mode == 'server') ? parsed.dir + '/' + parsed.name + '.server.ts' : path
        //we look for typescript package?
        var cwd = process.cwd();
        if (path.substr(0, cwd.length) != cwd) {
            path = this.repo + '/' + path;
        }
        return path;
    }
}
class fileInfo {
}
