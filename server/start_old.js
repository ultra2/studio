console.log("studio44 start...")

var modules = {}

process.on('message', (m) => {
  var message = m.message.split(':')
  var moduleName = message[0]
  var methodName = message[1]
  var data = m.data

  moduleName = moduleName.substr(moduleName.lastIndexOf('/')+1)

  if (!modules[moduleName]){
    var mod = require('./' + moduleName + '.server')
    modules[moduleName] = mod
  }

  var component = new modules[moduleName].default()
  component[methodName](data)
})
