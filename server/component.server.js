"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//#COMMON END
class Component {
    on(message, handler) {
        //window["socket"].on(message, handler.bind(this))
    }
    emit(message, data, component) {
        if (component)
            message = component + ":" + message;
        //process.send({ message: message, data: data })
        this.emitfn(message, data);
    }
    log(message) {
        this.emit("log", message, "main");
    }
}
exports.default = Component;
